<?php 
	include 'header.php';
	include 'koneksi.php';
	$transaksi = mysqli_query($koneksi, "SELECT * FROM transaksi");
	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($koneksi, "SELECT * FROM transaksi where kode_transaksi like '%$key%'");
	}
	else {
		$cari = $transaksi;
	}	
?>
<div class="container" style="margin-top:40px">
	<h2>Rekap Perhitungan Transaksi</h2>
<?php
$sql = 'SELECT id_pulsa, nama_pulsa, proveder, deskripsi_pulsa, harga_pokok,harga_jual ,kode_kategori, 
harga_jual*stok AS total 
		FROM pulsa';		
$query = mysqli_query($koneksi, $sql);

if (!$query) {
	die ('SQL Error: ' . mysqli_error($koneksi));
}
echo '<table>
		<thead>
		<table class="table table-striped table-hover table-sm table-bordered">
			<main role="main" class="col-md-9 col-lg-12 px-3">
				<form method="get" class="ml-2 mt-3">
					<table class="table table-striped table-sm w-100 p-3 ml-1 mt-3">
					<thead class="thead-light">
			<tr>
				<th scope="col">ID Pulsa</th>
				<th scope="col">Nama Pulsa</th>
				<th scope="col">Proveder</th>
				<th scope="col">Deskripsi Pulsa</th>
				<th scope="col">Harga Pokok</th>
				<th scope="col">Harga Jual</th>
				<th scope="col">Kode Kategori</th>
				<th scope="col">Total</th>
			</tr>
		</thead>
		<tbody>';		
while ($row = mysqli_fetch_array($query))
{
	echo '<tr>
			<td>'.$row['id_pulsa'].'</td>
			<td>'.$row['nama_pulsa'].'</td>
			<td>'.$row['proveder'].'</td>
			<td>'.$row['deskripsi_pulsa'].'</td>
			<td>'.$row['harga_pokok'].'</td>
			<td>'.$row['harga_jual'].'</td>
			<td>'.$row['kode_kategori'].'</td>
			<td>'.number_format($row['total'], 0, ',', '.').'</td>
		</tr>';
}
echo '
	</tbody>
</table>';
?>
</div>
<div class="container" style="margin-top:40px">
	<h2>Daftar Transaksi</h2>
	<hr>	
	<table class="table table-bordered table-striped">
            <div class="form-group row mt-2">
                <div class="col-sm-10">
                      <input type="text" name="keyword" class="form-control" id="Pencarian Kode Transaksi"></div>
                    <button type="submit" class="btn btn-sm btn-success" name="cari">Cari Kode Transaksi</button></div>
					<tr>
					</div>
					<table class="table table-striped table-sm w-100 p-3 ml-1 mt-3">
					<tr>
						<td>Total Transaksi :</td>
						<td scope="row">
							<?php echo $total = mysqli_num_rows($cari);?>
						</td>
					</tr>
				</table>
				</form>
				<table class="table table-bordered w-100 p-3 ml-1 mt-3">
					<thead class="thead-dark">
						<tr>
							<th scope="col">kode_transaksi</th>
							<th scope="col">tgl_transaksi</th>
							<th scope="col">total_hargabeli</th>
                            <th scope="col">status</th>
							<th scope="col">id_pulsa</th>
							<th scope="col">id_pelanggan</th>
							<th scope="col">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($cari as $value):?>
						<tr>
							<td><?php echo $value['kode_transaksi']; ?></td>
							<td><?php echo $value['tgl_transaksi']; ?></td>
							<td><?php echo $value['total_hargabeli']; ?></td>
							<td><?php echo $value['id_produk']; ?></td>
							<td><?php echo $value['id_pelanggan']; ?></td>
							<td>
								<a href="edit_transaksi.php?id=<?php echo $value['kode_transaksi'] ?>" class ="badge badge-warning">Edit</a>
								<a href="hapus_transaksi.php?id=<?php echo $value['kode_transaksi'] ?>" class="badge badge-danger">Hapus</a>
								<a href="tambah_transaksi.php" class ="badge badge-warning">Tambah Data</a>
							</td>
						</tr>
						<?php endforeach; ?></tbody>
				</table>
			</main></div></div></div>